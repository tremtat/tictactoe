import { CellState } from "./constants";

export class GameState {
  _state = [[null, null, null], [null, null, null], [null, null, null]];
  _update;
  _player = CellState.P1;
  isWun = false;

  static instance;

  static getInstance() {
    if (GameState.instance instanceof GameState) {
      return GameState.instance;
    } else {
      GameState.instance = new GameState();
      return GameState.instance;
    }
  }

  getState() {
    return this._state;
  }

  getPlayer() {
    return this._player;
  }

  subscribe(fn) {
    this._update = fn;
  }

  writeCellState({ x, y }) {
    this._state[x][y] = this._player;
    this.isWun = this.hasWon({ x, y }) ? this._player : false;
    this._player = this._player === CellState.P1 ? CellState.P2 : CellState.P1;
    this._update();
  }

  getCellState({ x, y }) {
    try {
      return this._state[x][y];
    } catch (err) {
      return undefined;
    }
  }

  hasWon({ x, y }) {
    let adjacentCells = [
      { x: -1, y: -1 },
      { x: 0, y: -1 },
      { x: -1, y: 0 },
      { x: 1, y: 1 },
      { x: 1, y: 0 },
      { x: 0, y: 1 }
    ];
    for (let i = 0; i < adjacentCells.length; i++) {
      const adjCell = adjacentCells[i];
      if (this.checkAdjacent({ x, y }, adjCell)) {
        return true;
      }
      continue;
    }
    return false;
  }

  checkAdjacent(coords, vector) {
    const cellState = this.getCellState(coords);
    const adjacentCoords = { x: coords.x + vector.x, y: coords.y + vector.y };
    const adjacent2Coords = {
      x: coords.x + vector.x * 2,
      y: coords.y + vector.y * 2
    };
    const adjacentState = this.getCellState(adjacentCoords);
    const adjacent2State = this.getCellState(adjacent2Coords);
    if (cellState === adjacentState && cellState === adjacent2State) {
      return true;
    }
    return false;
  }
}
