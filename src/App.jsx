import React, { Component } from "react";
import { Cell } from "./components/Cell";
import "./App.css";
import { GameState } from "./state";

class App extends Component {
  state = {
    matrix: GameState.getInstance().getState(),
    player: GameState.getInstance().getPlayer(),
    isWun: false
  };

  componentDidMount() {
    const state = GameState.getInstance();

    state.subscribe(() => {
      this.setState(prevState => {
        return {
          matrix: state.getState(),
          player: state.getPlayer(),
          isWun: state.isWun
        };
      });
    });
  }

  render() {
    const getCellState = ({ x, y }) => {
      return this.state.matrix[x][y];
    };
    const renderCell = ({ x, y }) => (
      <Cell coords={{ x, y }} cellState={getCellState({ x, y })} />
    );

    const renderRow = row => (
      <div style={{ display: "inline-block" }}>
        {renderCell({ x: row, y: 0 })}
        {renderCell({ x: row, y: 1 })}
        {renderCell({ x: row, y: 2 })}
      </div>
    );
    const renderMatrix = () => this.state.matrix.map((x, i) => renderRow(i));

    if (this.state.isWun) {
      window.alert(`${this.state.isWun} has won!!!`);
    }
    return (
      <div className="App">
        <p>It is {this.state.player}'s turn!</p>
        {renderMatrix()}
      </div>
    );
  }
}

export { App };
