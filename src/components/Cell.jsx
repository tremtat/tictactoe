import React from "react";
import { cellSize, CellState, Colors } from "../constants";
import { GameState } from "../state";

export function Cell(props) {
  const { coords, cellState } = props;

  const handleClick = () => {
    const stateInstance = GameState.getInstance();
    if (stateInstance.getCellState(coords) !== null) {
      return;
    }
    GameState.getInstance().writeCellState(coords);
  };
  const backgroundColor =
    cellState === CellState.P1
      ? Colors.P1
      : cellState === CellState.P2
      ? Colors.P2
      : null;

  return (
    <div
      style={{
        height: cellSize,
        width: cellSize,
        border: "1px solid black",
        backgroundColor
      }}
      onClick={handleClick}
    />
  );
}
