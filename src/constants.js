export const Colors = {
  P1: "red",
  P2: "green"
};

export const CellState = {
  P1: "Player 1",
  P2: "Player 2"
};

export const cellSize = 30;
